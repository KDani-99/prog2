package hu.unideb.prog2.customer;

import hu.unideb.prog2.product.Product;

public class CartItem {

    private final Product product;
    private int amount;

    private CartItem(Product product)
    {
        this.product = product;
        this.amount = 1;
    }

    public void increaseAmount()
    {
        this.amount += 1;
    }
    public void decreaseAmount()
    {
        this.amount -= 1;
    }

    public Product getProduct()
    {
        return this.product;
    }

    public static CartItem create(Product product)
    {
        return new CartItem(product);
    }

    public int getAmount() {
        return amount;
    }
}
