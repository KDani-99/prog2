package hu.unideb.prog2.customer;

import hu.unideb.prog2.product.Product;
import hu.unideb.prog2.product.ProductDatabase;
import hu.unideb.prog2.product.ProductType;

import java.util.ArrayList;
import java.util.List;

public class Cart {

    private static List<CartItem> items = new ArrayList<CartItem>();;

    public static void addItem(Product product)
    {
        int index = items.indexOf(product);
        if(index == -1)
            items.add(CartItem.create(product));
        else
            items.get(index).increaseAmount();
    }
    public static void removeItem(Product product)
    {
        int index = items.indexOf(product);
        if(index != -1)
        {
            var elem = items.get(index);
            if(elem.getAmount() == 1)
                items.remove(index);
            else
                elem.decreaseAmount();
        }
    }
    public static void checkout()
    {
        int priceSum = 0;
        System.out.println("-- Receipt --");
        for(var item : items)
        {
            ProductType temp = item.getProduct().getProductType();
            System.out.println(temp.toString());
            for(int i=0;i<item.getAmount();i++)
            {
                priceSum += temp.getPrice();
                //ProductDatabase.removeProduct(item.getProduct().getSerial());
            }
        }
        System.out.println("(Final) Price: "+priceSum);
        System.out.println("-- -------- --");
    }
}
