package hu.unideb.prog2;

import hu.unideb.prog2.command.CommandHandler;

public class Main {
    public static void main(String [] args)
    {
        CommandHandler.registerCommands();
        CommandHandler.readCommand();
    }
}
