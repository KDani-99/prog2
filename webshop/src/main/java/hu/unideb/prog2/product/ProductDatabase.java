package hu.unideb.prog2.product;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public abstract class ProductDatabase {

    private static List<ProductType> productTypes = new ArrayList<>();
    private  static List<Product> products = new ArrayList<>();

    public static void listProducts()
    {
        try
        {
            System.out.println("Products: "+products.size());
            for(Product product : products)
                System.out.println("\t"+product.toString());
        }
        catch (Exception ex)
        {
            System.out.println("Error: "+ex.toString());
        }
    }
    public static void listProductTypes()
    {
        try
        {
            System.out.println("Product types: "+productTypes.size());
            for(ProductType productType : productTypes)
                System.out.println("\t"+productType.toString());
        }
        catch (Exception ex)
        {
            System.out.println("Error: "+ex.toString());
        }
    }
    public static void importProductTypes(String path)
    {
        try
        {
            List<String> data = readFile(path);
            for(String elem : data)
            {
                String [] splittedData = elem.split(",");
                var category = splittedData[0];
                var brand = splittedData[1];
                var model = splittedData[2];
                var price = Integer.parseInt(splittedData[3]);
                addProductType(new ProductType(category,brand,model,price));
            }
        }
        catch (Exception ex)
        {
            System.out.println("Error: "+ex.toString());
        }
    }
    public static void importProducts(String path)
    {
        try
        {
            List<String> data = readFile(path);
            for(String elem : data)
            {
                String [] splittedData = elem.split(",");
                var productTypeUID = Integer.parseInt(splittedData[0]);
                var serial = splittedData[1];
                addProduct(productTypeUID,serial);
            }
        }
        catch (Exception ex)
        {
            System.out.println("Error: "+ex.toString());
        }
    }
    private static List<String> readFile(String path) throws IOException
    {
        List<String> lines;

        RandomAccessFile reader = new RandomAccessFile(path,"r");

        lines = new ArrayList<>((int)reader.length());

        String line;
        while((line = reader.readLine()) != null)
            lines.add(line);

        reader.close();

        return lines;
    }
    public static void addProductType(ProductType productType)
    {
        productTypes.add(productType);
        System.out.println("Added new ProductType: "+productType.toString());
    }
    public static void addProduct(int uid,String serial)
    {
        try
        {
            var product = productTypes.stream().filter(elem -> elem.getUid() == uid).findFirst().get();
            var temp = new Product(product,serial);
            products.add(temp);
            System.out.println("Added new Product: "+temp.toString());
        }
        catch (NoSuchElementException ex)
        {
            System.out.println("Product type with id `"+uid+"` does not exist.");
        }
    }
    public static void removeProduct(String serial)
    {
        products = products.stream().filter(elem -> !elem.getSerial().equals(serial)).collect(Collectors.toList());
        System.out.println(serial + " has been removed from the database (Product).");
    }
    public static void removeProductType(int uid)
    {
        var type = productTypes.stream().filter(elem -> elem.getUid() == uid).findFirst().get();
        productTypes.remove(type);
        System.out.println("Product type with uid: `"+uid + "` has been removed from the database (ProductType, and all of the products with the same type).");

        // remove every product with the same type
        products = products.stream().filter(elem -> !elem.getProductType().equals(type)).collect(Collectors.toList());

    }
    public static Product getProductByProductTypeUID(int uid) throws Exception
    {
        var product = products.stream().filter(elem -> elem.getProductType().getUid() == uid).findFirst().get();
        if(product == null) throw new Exception(uid+" is out of stock.");
        return product;
    }
}
