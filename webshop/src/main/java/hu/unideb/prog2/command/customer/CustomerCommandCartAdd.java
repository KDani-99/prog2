package hu.unideb.prog2.command.customer;

import hu.unideb.prog2.command.AbstractCommand;
import hu.unideb.prog2.customer.Cart;
import hu.unideb.prog2.product.ProductDatabase;

import java.util.List;

public class CustomerCommandCartAdd extends AbstractCommand {
    public CustomerCommandCartAdd()
    {
        super("customer","cart","add");
    }

    @Override
    public void use(List<String> args) throws Exception{

        if(args.size() != 2) throw new Exception("Invalid command. Usage: "+this.usage());

        var uid = Integer.parseInt(args.get(0));
        var amount = Integer.parseInt(args.get(1));

        for(int i=0;i<amount;i++)
        {
            var productTemp = ProductDatabase.getProductByProductTypeUID(uid);
            Cart.addItem(productTemp);
            ProductDatabase.removeProduct(productTemp.getSerial()); // removing from cart should add it back (not implemented feature)
        }
    }

    @Override
    public String usage() {
        return this.group + " " + this.entityType + " " + this.action + " <product type uid> <amount>";
    }
}
