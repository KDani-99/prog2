package hu.unideb.prog2.command.customer;

import hu.unideb.prog2.command.AbstractCommand;
import hu.unideb.prog2.customer.Cart;
import hu.unideb.prog2.product.ProductDatabase;

import java.util.List;

public class CustomerCommandCartCheckout extends AbstractCommand {
    public CustomerCommandCartCheckout()
    {
        super("customer","cart","checkout");
    }

    @Override
    public void use(List<String> args) throws Exception{

        if(args.size() != 0) throw new Exception("Invalid command. Usage: "+this.usage());

        Cart.checkout();
    }

    @Override
    public String usage() {
        return this.group + " " + this.entityType + " " + this.action;
    }
}
