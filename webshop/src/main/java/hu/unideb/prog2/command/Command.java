package hu.unideb.prog2.command;

import java.util.List;

public interface Command {
    void use(List<String> args) throws Exception;
    String usage();
}
