package hu.unideb.prog2.command.admin;

import hu.unideb.prog2.command.AbstractCommand;
import hu.unideb.prog2.product.Product;
import hu.unideb.prog2.product.ProductDatabase;
import hu.unideb.prog2.product.ProductType;

import java.util.List;

public class AdminCommandProductAdd extends AbstractCommand {
    public AdminCommandProductAdd()
    {
        super("admin","product","add");
    }

    @Override
    public void use(List<String> args) throws Exception{

        if(args.size() != 2) throw new Exception("Invalid command. Usage: "+this.usage());

        var uid = Integer.parseInt(args.get(0));
        var serial = args.get(1);

        ProductDatabase.addProduct(uid,serial);
    }

    @Override
    public String usage() {
        return this.group + " " + this.entityType + " " + this.action + " <product type uid> <serial>";
    }
}
