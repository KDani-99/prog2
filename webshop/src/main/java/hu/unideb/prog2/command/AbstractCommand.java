package hu.unideb.prog2.command;

public abstract class AbstractCommand implements Command{

    public String getGroup() {
        return group;
    }

    public String getEntityType() {
        return entityType;
    }

    public String getAction() {
        return action;
    }


    protected String group;
    protected String entityType;
    protected String action;

    public AbstractCommand(String group, String entityType, String action)
    {
        this.group = group;
        this.entityType = entityType;
        this.action = action;
    }

}
