package hu.unideb.prog2.command.admin;

import hu.unideb.prog2.command.AbstractCommand;
import hu.unideb.prog2.product.ProductDatabase;
import hu.unideb.prog2.product.ProductType;

import java.util.List;

public class AdminCommandProductRemove extends AbstractCommand {
    public AdminCommandProductRemove()
    {
        super("admin","product","remove");
    }

    @Override
    public void use(List<String> args) throws Exception{

        if(args.size() != 1) throw new Exception("Invalid command. Usage: "+this.usage());

        var serial = args.get(0);

        ProductDatabase.removeProduct(serial);
    }

    @Override
    public String usage() {
        return this.group + " " + this.entityType + " " + this.action + " <serial>";
    }
}
