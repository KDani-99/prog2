package hu.unideb.prog2.command.admin;

import hu.unideb.prog2.command.AbstractCommand;
import hu.unideb.prog2.product.ProductDatabase;
import hu.unideb.prog2.product.ProductType;

import java.util.List;

public class AdminCommandProductTypeAdd extends AbstractCommand {
    public AdminCommandProductTypeAdd()
    {
        super("admin","product_type","add");
    }

    @Override
    public void use(List<String> args) throws Exception{

        if(args.size() != 4) throw new Exception("Invalid command. Usage: "+this.usage());

        var category = args.get(0);
        var brand = args.get(1);
        var model = args.get(2);
        var price = Integer.parseInt(args.get(3));

        ProductDatabase.addProductType(new ProductType(category,brand,model,price));
    }

    @Override
    public String usage() {
        return this.group + " " + this.entityType + " " + this.action + " <category> <brand> <model> <price>";
    }
}
