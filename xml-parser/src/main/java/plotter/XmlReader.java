package plotter;

import java.util.List;

public interface XmlReader { // XmlReader interface
    List<City> getCities();
}
