package plotter;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

import java.awt.*;
import java.io.Writer;

public class SvgMapImage implements MapImage { // Implementálja a MapImage interface-t, amely az addPoint és save metódust tartalmazza

    SVGGraphics2D svgGenerator;

    public SvgMapImage(int canvasWidth, int canvasHeight) {

        DOMImplementation dom = GenericDOMImplementation.getDOMImplementation(); // dom implementáció betöltése

        String www = "http://www.w3.org/2000/svg";
        Document document = dom.createDocument(www, "svg", null);

        svgGenerator = new SVGGraphics2D(document); // Svg generátor példány elkészítése
        svgGenerator.setSVGCanvasSize(new Dimension(canvasWidth, canvasHeight));  // canvas méretének beállítása
    }

    @Override
    public void addPoint(double x, double y) {
        svgGenerator.fillOval((int) x, (int) y, 2, 2);
    }

    @Override
    public void save(Writer targetStream) {
        try {
            svgGenerator.stream(targetStream); // a megadott targetStream-re írjuk
        } catch (Exception e) {
            throw new RuntimeException("Failed to write image", e);
        }
    }
}
