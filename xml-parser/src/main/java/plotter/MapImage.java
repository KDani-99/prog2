package plotter;

import java.io.Writer;

public interface MapImage { // Interface a MapImage-hez, ami az addPoint és a save metódust tartalmazza
    void addPoint(double x, double y);

    void save(Writer targetStream);
}
