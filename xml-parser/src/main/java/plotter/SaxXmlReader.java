package plotter;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

public class SaxXmlReader extends DefaultHandler implements XmlReader { // Implementálja az XmlReader interface-t és örököl a DefaultHandler osztályból

    private double currentXCoordinate; // jelenlegi X koordináta
    private double currentYCoordinate; // jelenlegi Y koordináta
    private String currentState; // jelenlegi állapot

    private Consumer<String> actionOnCharacters = c -> { // consumer készítése
    };

    private List<City> result = new LinkedList<>(); // az eredményt egy láncolt listába mentjük

    public SaxXmlReader(InputStream input) throws ParserConfigurationException, SAXException, IOException {
        SAXParser parser = SAXParserFactory.newInstance().newSAXParser(); // a factory-val készítünk egy új példányt
        parser.parse(input, this); // az inputstream-re parsoljuk
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("coordinateX")) {
            actionOnCharacters = c -> currentXCoordinate = Double.parseDouble(c); // consumerrel koordinátát dobule-ra parsoljuk
        }
        if (qName.equalsIgnoreCase("coordinateY")) {
            actionOnCharacters = c -> currentYCoordinate = Double.parseDouble(c);  // consumerrel koordinátát dobule-ra parsoljuk
        }
        if (qName.equalsIgnoreCase("state")) {
            actionOnCharacters = c -> currentState = c;  // string
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("city")) {
            result.add(new City(currentXCoordinate, currentYCoordinate, currentState));
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        actionOnCharacters.accept(new String(ch, start, length));
        actionOnCharacters = c -> {
        };
    }

    @Override
    public List<City> getCities() {
        return result;
    } // visszatérünk az eredménnyel
}
