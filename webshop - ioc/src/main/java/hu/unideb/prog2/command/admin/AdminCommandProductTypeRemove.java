package hu.unideb.prog2.command.admin;

import hu.unideb.prog2.command.AbstractCommand;
import hu.unideb.prog2.product.ProductDatabase;

import java.util.List;

public class AdminCommandProductTypeRemove extends AbstractCommand {
    public AdminCommandProductTypeRemove()
    {
        super("admin","product_type","remove");
    }

    @Override
    public void use(List<String> args) throws Exception{

        if(args.size() != 1) throw new Exception("Invalid command. Usage: "+this.usage());

        var uid = Integer.parseInt(args.get(0));

        ProductDatabase.removeProductType(uid);
    }

    @Override
    public String usage() {
        return this.group + " " + this.entityType + " " + this.action + " <uid>";
    }
}
