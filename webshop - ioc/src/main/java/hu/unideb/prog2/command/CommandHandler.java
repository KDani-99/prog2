package hu.unideb.prog2.command;

import hu.unideb.prog2.Logger.ILogger;
import hu.unideb.prog2.command.admin.*;
import hu.unideb.prog2.command.customer.CustomerCommandCartAdd;
import hu.unideb.prog2.command.customer.CustomerCommandCartCheckout;

import java.util.*;
import java.util.stream.Collectors;

public class CommandHandler {

    private static HashMap<String,List<AbstractCommand>> availableCommands = new HashMap<>();
    private static ILogger logger;

    public static void init(ILogger logger)
    {
        CommandHandler.logger = logger;
        CommandHandler.registerCommands();
        CommandHandler.readCommand();
    }

    private static void addCommand(AbstractCommand cmd)
    {
        if(!availableCommands.containsKey(cmd.getGroup()))
        {
            List<AbstractCommand> cmds = new ArrayList<>();
            cmds.add(cmd);
            availableCommands.put(cmd.getGroup(),cmds);
        }
        else
            availableCommands.get(cmd.getGroup()).add(cmd);
    }

    public static void listCommands()
    {
        var groups = availableCommands.keySet();
        for(var group : groups)
        {
            System.out.println("Commands available for group: `"+group+"`");
            for(var cmd : availableCommands.get(group))
                System.out.println("\t"+cmd.usage());
            System.out.println("");
        }
    }

    private static void parseCommand(String cmd)
    {
        try
        {
            List<String> cmdData = Arrays.stream(cmd.split(" ")).collect(Collectors.toList());

            if(cmdData.size() == 1 && cmdData.get(0).toLowerCase().equals("list"))
            {
                listCommands();
                return;
            }

            if(cmdData.size() < 3) throw new Exception("Invalid command");

            var group = cmdData.remove(0);
            var entityType = cmdData.remove(0);
            var action = cmdData.remove(0);

            var matchingEntityTypes = availableCommands.get(group).stream().filter(elem -> elem.entityType.equals(entityType)).collect(Collectors.toList());

            var cmdToUse = matchingEntityTypes.stream().filter(elem -> elem.action.equals(action)).findFirst().get();

            try
            {
                cmdToUse.use(cmdData);
            }
            catch (Exception ex)
            {
                System.out.println(ex.toString());
            }
        }
        catch (NoSuchElementException ex)
        {
            System.out.println("Invalid command");
        }
        catch (Exception ex)
        {
            System.out.println("Error: (parseCommand) "+ex.toString());
        }
    }
    public static void readCommand()
    {
        Scanner reader = new Scanner(System.in);
        String cmd;
        System.out.println("Waiting for user input...");
        while(!(cmd = reader.nextLine()).equals("exit"))
            parseCommand(cmd);
        System.out.println("Exiting program...");
    }

    public static void registerCommands()
    {
        var adminCommandImportProductTypes = new AdminCommandImportProductTypes();
        addCommand(adminCommandImportProductTypes);

        var adminCommandImportProducts = new AdminCommandImportProducts();
        addCommand(adminCommandImportProducts);

        var adminCommandProductList = new AdminCommandProductList();
        addCommand(adminCommandProductList);

        var adminCommandProductTypeList = new AdminCommandProductTypeList();
        addCommand(adminCommandProductTypeList);

        var adminCommandProductAdd = new AdminCommandProductAdd();
        addCommand(adminCommandProductAdd);

        var adminCommandProductTypeAdd = new AdminCommandProductTypeAdd();
        addCommand(adminCommandProductTypeAdd);

        var adminCommandProductRemove = new AdminCommandProductRemove();
        addCommand(adminCommandProductRemove);

        var adminCommandProductTypeRemove = new AdminCommandProductTypeRemove();
        addCommand(adminCommandProductTypeRemove);

        var customerCommandCartAdd = new CustomerCommandCartAdd();
        addCommand(customerCommandCartAdd);

        var customerCommandCartCheckout = new CustomerCommandCartCheckout();
        addCommand(customerCommandCartCheckout);
    }

}
