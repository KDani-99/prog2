package hu.unideb.prog2.command.admin;

import hu.unideb.prog2.command.AbstractCommand;
import hu.unideb.prog2.product.ProductDatabase;

import java.util.List;

public class AdminCommandImportProducts extends AbstractCommand {

    public AdminCommandImportProducts()
    {
        super("admin","product","import");
    }

    @Override
    public void use(List<String> args) throws Exception{

        if(args.size() != 1) throw new Exception("Invalid command. Usage: "+this.usage());

        var path = args.get(0);
        ProductDatabase.importProducts(path);
    }

    @Override
    public String usage() {
        return this.group + " " + this.entityType + " " + this.action + " " + "<path/to/file.csv>";
    }
}