package hu.unideb.prog2.command.admin;

import hu.unideb.prog2.command.AbstractCommand;
import hu.unideb.prog2.product.ProductDatabase;

import java.util.List;

public class AdminCommandProductList extends AbstractCommand {
    public AdminCommandProductList()
    {
        super("admin","product","list");
    }

    @Override
    public void use(List<String> args) throws Exception{

        if(args.size() != 0) throw new Exception("Invalid command. Usage: "+this.usage());

        ProductDatabase.listProducts();
    }

    @Override
    public String usage() {
        return this.group + " " + this.entityType + " " + this.action;
    }
}
