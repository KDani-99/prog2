package hu.unideb.prog2.product;

import hu.unideb.prog2.annotations.CustomProduct;

import java.util.Objects;

@CustomProduct
public class Product {
    private ProductType productType;
    private String serial;

    public Product(ProductType productType,String serial)
    {
        this.productType = productType;
        this.serial = serial;
    }

    public ProductType getProductType() {
        return productType;
    }

    public String getSerial() {
        return serial;
    }

    @Override
    public String toString() {
        return "Product [serial="+serial+", "+productType.toString()+"]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(productType, product.productType) &&
                Objects.equals(serial, product.serial);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productType, serial);
    }
}
