package hu.unideb.prog2.Logger;

import hu.unideb.prog2.product.Product;
import hu.unideb.prog2.product.ProductDatabase;
import hu.unideb.prog2.product.ProductType;

import java.lang.reflect.*;
import java.util.List;

public class Logger implements ILogger {

    @Override
    public void Log() throws NoSuchFieldException, IllegalAccessException {

        var productTypes = ProductDatabase.class.getDeclaredField("productTypes");
        productTypes.setAccessible(true); // private

        var value = (List<ProductType>)productTypes.get(null); //castoljuk

        System.out.println("=== LOG ===");

        System.out.println("Products: "+value.size());

        for(ProductType productType : value)
        {
            var fields = productType.getClass().getDeclaredFields(); // public, private, protected, etc. (minden field)
            for(var field : fields) // vegigmegyunk minden fielden
            {
                // Minden field kiiratasa (akkor lesz meghivva a log, amikor uj productot tarolunk el a Database-ba)
                field.setAccessible(true); // mivel private minden mezo
                System.out.println(field.getName() + " = " + field.get(productType));
            }
        }

        System.out.println("===========");
    }
}
