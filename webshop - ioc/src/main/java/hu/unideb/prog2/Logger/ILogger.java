package hu.unideb.prog2.Logger;

public interface ILogger {
    void Log() throws NoSuchFieldException, IllegalAccessException;
}
