package hu.unideb.prog2.annotations;

import hu.unideb.prog2.product.ProductType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CustomProduct {
    public String serial() default "<unknown>";
}
