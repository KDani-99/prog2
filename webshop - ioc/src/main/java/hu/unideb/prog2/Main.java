package hu.unideb.prog2;

import hu.unideb.prog2.Logger.Logger;
import hu.unideb.prog2.command.CommandHandler;

public class Main {

    public static void main(String [] args)
    {
        Logger logger = new Logger();
        CommandHandler.init(logger);
    }
}
