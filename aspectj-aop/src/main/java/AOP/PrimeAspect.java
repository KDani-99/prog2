package AOP;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import java.util.ArrayList;
import java.util.List;

@Aspect
public class PrimeAspect {

    public static void main(String [] args)
    {
        new Prime().getPrimeNumbers(5000);
    }

    @Around("execution(public java.util.List<Integer> Prime.getPrimeNumbers(int))")
    public Object getNumbers(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.currentTimeMillis();

         List<Integer> numbers = (List<Integer>)joinPoint.proceed();

        long elapsed = System.currentTimeMillis() - start;

         System.out.println("Elapsed time (Around w/ AspectJ only) : " + elapsed + " (ms).\nList size: "+numbers.size());

         return numbers;
    }
}
