package AOP;

import org.aspectj.lang.annotation.Aspect;

import java.util.ArrayList;
import java.util.List;

public class Prime {
    public List<Integer> getPrimeNumbers(int n)
    {
        ArrayList<Integer> primes = new ArrayList<Integer>();

        for(int i=0;i<Integer.MAX_VALUE;i++)
        {
            if(primes.size() == n) break;
            if(isPrimeNumber(i)) primes.add(i);
        }

        return primes;
    }
    private static boolean isPrimeNumber(int number)
    {
        if(number <= 1) return false;

        for(int i=2;i<number / 2;i++)
        {
            if(number % i == 0) return false;
        }

        return true;
    }
}
