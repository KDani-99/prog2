package AOP;

import java.util.List;

public aspect PrimeAspect {
    pointcut getNumbers(int n,Prime prime) : execution(List<Integer> Prime.getPrimeNumbers(int) && args(n) && target(prime));//call(List<Integer> Prime.getPrimeNumbers(int) && args(n) && target(prime));

    around(int n, Prime prime) : getNumbers(n,prime) {

        long start = System.currentTimeMillis();

       // List<Integer> numbers = (List<Integer>)proceed(n,prime);

        long elapsed = System.currentTimeMillis() - start;

       // System.out.println("Elapsed time: " + elapsed + " (ms).\nList size: "+numbers.size());
    }
}
