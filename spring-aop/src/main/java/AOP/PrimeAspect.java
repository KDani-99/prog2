package AOP;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.List;

@Aspect
@Service
public class PrimeAspect {
    @Around("execution(public java.util.List<Integer> getPrimeNumbers(int))")
    public void getPrimeBefore(ProceedingJoinPoint joinPoint) throws Throwable {
        StopWatch sw = new StopWatch();
        sw.start();

        List<Integer> numbers = (List<Integer>)joinPoint.proceed();

        sw.stop();

        System.out.println("Elapsed time: " + sw.getTotalTimeMillis() + " (ms).\nList size: "+numbers.size());
    }
}
