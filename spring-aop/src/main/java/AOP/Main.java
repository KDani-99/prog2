package AOP;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class Main {
    public static void main(String [] args)
    {
        SpringApplication.run(Main.class, args);
       // var numbers = Prime.getPrimeNumbers(10);
        //numbers.forEach(elem -> System.out.println(elem));
    }
    @Bean
    public CommandLineRunner commandLineRunner(Prime primeProvider) {
        return args -> primeProvider.getPrimeNumbers(5000);
    }
}
